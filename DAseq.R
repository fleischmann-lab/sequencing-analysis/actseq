devtools::install_github("KlugerLab/DAseq")

## DAseq
library(DAseq)

X.pca <- read.table('neurons_pca_harmony.csv',header=TRUE,sep=',',row.names='X')
X.umap <- read.table('neurons_umap_harmony.csv',header=TRUE,sep=',',row.names='X')

#testing with their data?
X.label.info <- X.label.info
X.melanoma <- X.melanoma
X.label.melanoma <- X.label.melanoma
labels_res <- X.label.info[X.label.info$condition == "R", "label"]

#cell.labels <- read.table('neurons_metadata.csv',header=TRUE,sep=',',row.names='X')
cell.labels <- read.csv('neurons_metadata.csv',row.names = 1)
#cell.labels <- cell.labels['condition']

#condition
labels_sucrose <- row.names(cell.labels)[which(cell.labels$condition=="sucrose")]
labels_quinine <- row.names(cell.labels)[which(cell.labels$condition=="quinine")]
labels_adlib <- which(cell.labels$condition=="adlib")
labels_noodor <- which(cell.labels$condition=="no_odor")

da_cells <- getDAcells(
  X = X.pca,
  #cell.labels = row.names(cell.labels),
  cell.labels = cell.labels$condition,
  labels.1 = c('sucrose'),  #change how you split labels.1 and labels.2 for your desired comparison
  labels.2 = c('no_odor','adlib','quinine'), 
  k.vector = seq(50,500,50),
  plot.embedding = X.umap
)

#plot results on umap
da_cells$pred.plot

#check random permutation to generate null distribution (to set cutoffs for selected DA cells)
da_cells$rand.plot

#plot selected DAcells on umap (blue is label_1)
da_cells$da.cells.plot

## Get DA regions
#In this step, selected DA cells will be clustered into several coherent regions, which represent the DA cell subpopulations.
da_regions <- getDAregion(
  X = X.pca,
  da.cells = da_cells,
  cell.labels = cell.labels$condition,
  labels.1 = c('sucrose'), #want labels.1 and labels.2 here to match above
  labels.2 = c('no_odor','adlib','quinine'),
  resolution = 0.001,
  plot.embedding = X.umap
)

#plot clustering result on umap
da_regions$da.region.plot

#save da_regions
write.csv(da_regions[c('cell.idx','da.region.label')],'da_regions_sucrose_vs_rest.csv') #can change filename to be whatever you want

## Get markers for each DA subpopulation with STG
#The final step of DAseq is to characterize each DA subpopulation by detecting genes that seprate the DA subpopulation from the rest of the cells through STG (stochastic gates).
# X.expression <- read.table('neurons_gene_expression.csv',header=TRUE,sep=',',row.names='X')
# X.ieg_expression <- t(read.table('neurons_ieg_expression.csv',header=TRUE,sep=',',row.names='X',stringsAsFactors = F))
# 
# python2use <- 'C:/tools/Anaconda3/envs/DAseq/python.exe'
# STG_markers <- STGmarkerFinder(
#   X = as.matrix(X.ieg_expression),
#   da.regions = da_regions,
#   da.regions.to.run = c(1,2), #only some of them for testing
#   lambda = 1.5, n.runs = 1, return.model = F,
#   python.use = python2use
# )
# STG_markers
