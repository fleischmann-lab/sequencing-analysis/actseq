# Act-Seq Analysis

This project performs quality control, integration with Harmony, clustering, subclustering, and visualization of Act-Seq data.

Analysis happens in ActSeq.ipynb. All other files are auxiliary to that notebook (see descriptions of each file below).

**ActSeq.ipynb** 
This is the main analysis notebook. Follow the documentation to read in your data, integrate with Harmony, cluster, subcluster, and visualize IEG expression and differences between bored and active conditions.

**ActSeq_functions.py** This python file (run from within ActSeq.ipynb) defines and documents functions used in ActSeqipynb to avoid cluttering that notebook. You should not need to make edits to this file, but should look at for documentation of the functions used in ActSeq.ipynb. Each function is accompanied by an explanataion of its purpose, inputs, and outputs.

**filenames.txt** This text file define filenames that ActSeq.ipynb uses to read inputs and save outputs. This way, difference users or projects can change store their data in different places without changing the main notebook, as long as they have a correct filenames.txt file. The file uses the format

> name_key_used_in_notebook: filepath 

The file provided in this repository is an example with the expected formatting and all name keys used in the main notebook. Edit your local version to reflect the desired filepaths on your machine.

**actseq.yml** This yml file describes the recommended conda environment. You can use this to create an identical conda environment on your machine.

<br>
<br>

This project was put together by Robin Attey (robin_attey@brown.edu | robin.attey@gmail.com). Many functions were taken from or inspired by code by Anton Crombach,  as indicated in ActSeq_functions.py.