import numpy as np
import pandas as pd
import scanpy as sc
import bisect
from sklearn.metrics import silhouette_score
import matplotlib.pyplot as plt
import seaborn as sns

'''
mitochondrial filtering (adapted from code by Anton Chrombach)

arguments:
    data: AnnData object
    MITO_CUTOFF: float (cutoff for mitochondrial filtering)
'''
def mito_filter(data,MITO_CUTOFF):

    mito_genes = [name for name in data.var_names if name.startswith(('mt-', 'MT-'))]
    data.obs['frac_mito'] = np.sum(data[:, mito_genes].X, axis=1).A1 / np.sum(data.X, axis=1).A1
    data.obs['n_counts'] = data.X.sum(axis=1).A1
    data.obs['n_genes'] = np.sum(data.X > 0, axis=1).A1
    data.var['n_cells'] = np.sum(data.X > 0, axis=0).A1

    # Store the cutoff value in the "unsorted annotations" of the AnnData object
    data.uns['mito_cutoff'] = MITO_CUTOFF

    ## plotting some results
    min_count = 0
    fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(10, 10));
    sns.despine(fig)

    # Plot the fraction of mito expression per total expression in a cell
    sns.scatterplot(x=data.obs['n_counts'], y=data.obs['frac_mito'], s=15, alpha=0.5, ax=axs[0, 0]);
    axs[0, 0].set(xlabel='Total counts / cell', ylabel='Fraction mito counts / cell');
    #sc.pl.scatter(data, x='n_counts', y='frac_mito', ax=axs[0, 0]);


    axs[0, 0].axhline(y=MITO_CUTOFF);

    # Plot the fraction of mito expression per nr of expressed genes in a cell
    sns.scatterplot(x=data.obs['n_genes'], y=data.obs['frac_mito'], s=15, alpha=0.5, ax=axs[0, 1]);
    axs[0, 1].set(xlabel='Genes with expression / cell', ylabel='Fraction mito counts / cell');
    #sc.pl.scatter(data, x='n_genes', y='percent_mito', ax=axs[0, 1]);
    axs[0, 1].axhline(y=MITO_CUTOFF);

    # Plot nr of expressed genes against total expression
    sns.scatterplot(x=data.obs['n_genes'], y=data.obs['n_counts'], s=15, alpha=0.5, ax=axs[1, 0])
    axs[1, 0].set(xlabel='Genes with expression / cell', ylabel='Total counts / cell');
    #sc.pl.scatter(data, x='n_genes', y='n_counts', ax=axs[1, 0]);

    # Plot distribution of nr of cells in which a gene was (detected to be) expressed
    aux = data.var['n_cells'][data.var['n_cells'] >= min_count]
    sns.distplot(aux, bins=50, kde=False, ax=axs[1, 1]);
    axs[1, 1].set(xlabel='Gene detected in $n$ cells', ylabel='Nr of cells');

    n_dying = np.sum(data.obs['frac_mito'] >= MITO_CUTOFF)
    n_alive = np.sum(data.obs['frac_mito'] < MITO_CUTOFF)
    print(f"Dying: {n_dying}\nAlive: {n_alive}")

    return data[data.obs['frac_mito'] < MITO_CUTOFF, :]


'''
Extract IEGs from the data matrix and store them in obs instead. This way, IEG expression is not used as the basis for clustering, but values are retained for refence/downstream analysis.

Arguments:
    ad: AnnData object
    iegs: list of IEGs to extract
'''
def extract_IEGs(ad, iegs):
    # get data into pandas dataframe so can extract IEG levels
    # have to check whether matrix is sparse or dense (already numpy)
    if isinstance(ad.X, np.ndarray):
        df = pd.DataFrame(ad.X,index=ad.obs.index,columns=ad.var_names)
    else:
        df = pd.DataFrame(ad.X.todense(),index=ad.obs.index,columns=ad.var_names)

    #save iegs in obs
    for gene in iegs:
        if gene in df.columns:
            ad.obs[gene] = df[gene]

    #also save an agregate measure (via a built in scanpy method - see method at https://scanpy.readthedocs.io/en/stable/generated/scanpy.tl.score_genes.html#scanpy.tl.score_genes)
    sc.tl.score_genes(ad,iegs,score_name='IEG_score')

    #remove iegs from main matrix
    non_ieg = [gene for gene in ad.var_names if gene not in iegs]
    ad = ad[:, non_ieg]

    return ad


'''
Marking the top highly variable genes for each replicate (written by Anton Crombach)

Arguments:
    data: AnnData object
    ignore_list: list of genes to exclude from HVGs (for example, IEGs)
    min_mean (optional): minimal level of gene expression to consider (default: 0.0)
    max_mean (optional): maximal level of gene expression (default: 1000.0)
    min_disp (optional) minimal level of dispersion to consider (default: 0.05)
    n_bins (optional): number of bins to divide the distribution of expression levels over. 
            The bins go from low mean expression levels to high ones (default: 50)
    batch (optional): which obs column to use to separate data (default: 'replicate')
    dry_run (optional): if True, compute, plot, and return the results, but don't make any modifications to the AnnData object (this is useful for tuning parameters). 
            If False, save the results in the AnnData object (default: True)
'''
def mark_highly_variable_genes_per_replicate(data, ignore_list, min_mean=0.0, max_mean=100.0, min_disp=0.05,
                                             n_bins=50, batch='replicate', dry_run=True):
    
    old_verbo = sc.settings.verbosity
    # I want lots of feedback from the algorithm
    if dry_run:
        sc.settings.verbosity = 4
        filter_result = sc.pp.highly_variable_genes(data, 
                                                    min_mean=min_mean, max_mean=max_mean, 
                                                    min_disp=min_disp, max_disp=20.0,
                                                    n_bins=n_bins, 
                                                    batch_key=batch,
                                                    inplace=False)
    
        # Plot the filtering result, even if I do not find the plots very intuitive.
        sc.pl.highly_variable_genes(filter_result)
        sc.settings.verbosity = old_verbo
        return filter_result
    
    else:
        # Do the actual filtering
        sc.pp.highly_variable_genes(data, 
                                    min_mean=min_mean, max_mean=max_mean, 
                                    min_disp=min_disp, max_disp=20.0,
                                    n_bins=n_bins,
                                    batch_key=batch)

        # If we have an ignore list, set HVG properties to zero and False
        data.var.loc[[name for name in data.var_names if name in ignore_list], ['highly_variable_nbatches', 'highly_variable_intersection', 'highly_variable']] = (0, False, False)
    return data


'''
Selecting only the top highly variable genes (written by Anton Crombach)

Arguments:
    data: AnnData object
    top (optional): integer of how many genes to select (default: 2000)
'''
def select_top_n_highly_variable_genes(data, top=2000):
    aux = data.var[data.var['highly_variable']]['highly_variable_nbatches'].value_counts()
    hvg_nbatch = sorted(aux.to_dict().items(), key=lambda x: x[0], reverse=True)
    
    # Linear search for how many nbatches we need
    sum_ngenes = np.cumsum([x[1] for x in hvg_nbatch])
    print(hvg_nbatch)
    if top > sum_ngenes[-1]:
        print("Cannot select more genes than marked as highly variable.")
        return data
    i_nbatch = bisect.bisect(sum_ngenes, top)
    
    # Set all genes to False
    data.var['highly_variable'] = False
    
    # Take the full nbatches
    if i_nbatch > 0:
        data.var['highly_variable'] = data.var['highly_variable_nbatches'] > hvg_nbatch[i_nbatch][0]
        top -= sum_ngenes[i_nbatch - 1]
        
    # Take a partial nbatch, sorted by normalized dispersion
    aux = data.var[data.var['highly_variable_nbatches'] == hvg_nbatch[i_nbatch][0]]['dispersions_norm']
    # Note: we could use np.argpartition here to speed things up if needed
    data.var.loc[aux.sort_values()[-top:].index, 'highly_variable'] = True
    
    return data


'''
Split replicates (adapted from code written by Anton Crombach)

Arguments:
    data: AnnData object
    batch (optional): which column of obs to group observations by (default: 'batch)

Returns: list of AnnData objects
'''
def __split_replicates(data, batch='replicate'):
    # Heavily inspired on scib package of Theis lab
    split = []
    batch_categories = data.obs[batch].unique()
    for b in batch_categories:
        split.append(data[data.obs[batch] == b].copy())
    return split

'''
Merge the split replicates back together(adapted from code written by Anton Crombach)

Arguments:
    data: AnnData object
    batch (optional): which column of obs observations were grouped by (default: 'batch)

Returns: AnnData object
'''
def __merge_replicates(data_list, batch='replicate'):
    # The new ad.concat does not take into account the many var and obs fields
    data = data_list[0].concatenate(*data_list[1:], batch_key='aux', uns_merge='same', index_unique=None)
    # 'aux' was only a temporary column, we already have batch saved
    del data.obs['aux']

    # Removing 'mean-#' and 'std-#' columns... Or are they going to be useful?
    for i in range(len(data.obs[batch].unique())):
        del data.var['mean-{}'.format(i)]
        del data.var['std-{}'.format(i)]
    return data


'''
Compute and store everything needed to compare different clustering parameters

Arguments:
    data: AnnData object
    embedding: 'pca' or 'harmony'
    seed: integer seed to use for UMAP
    n_neighbors (optional): list of values to try as the number of neighbors (default: [5, 10, 20, 50, 100])
    resolutions (optional): list of values to try as the resolution (default: [0.2, 0.5, 0.9])

Returns: data_cache (dictionary storing all computed values for all parameters)
'''
def compare_clusterings_compute(data,embedding,seed,n_neighbors = [50,100],resolutions = [0.2,0.5,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.8,3,4]):
    #reduce scanpy's output setting, otherwise will get TONS of output from this function
    sc.settings.verbosity = 1 

    #which embedding
    if embedding == 'pca':
        X_name = 'X_pca'   
        dendrogram_name = 'dendrogram_leiden_noharmony'  
        
    elif embedding == 'harmony':
        X_name = 'X_pca_harmony'
        dendrogram_name = 'dendrogram_leiden_harmony'        

    else:
        raise ValueError("Unknown embedding! must be either 'pca' or 'harmony'")
    
    nb_name = 'neighbors'
    distances_name = 'distances'
    connectivities_name = 'connectivities'

    # Make a grid of neighbors and leiden clustering
    data_cache = {
        'embedding': embedding, 
        'n_neighbors': n_neighbors, #storing the number of neighbors and resolutions that were tried so plotting doesn't need them as a separate argument
        'resolutions': resolutions,
        'uns_neighbors': {}, 
        'obsp_distances': {}, 
        'obsp_connectivities': {},
        'leiden': {},
        'silhouette_score': {},
        'obsm_X_umap': {},
        'dendrogram': {}
    }

    #computing and storing
    for nb in n_neighbors:
        sc.pp.neighbors(data, n_pcs=data.uns['neighbors']['params']['n_pcs'], n_neighbors=nb, use_rep=X_name)
    
        # Store the results of the neighborhood computation
        data_cache['uns_neighbors'][nb] = data.uns[nb_name].copy()
        data_cache['obsp_distances'][nb] = data.obsp[distances_name].copy()
        data_cache['obsp_connectivities'][nb] = data.obsp[connectivities_name].copy()

        # Compute UMAP projection, store it
        sc.tl.umap(data, random_state=seed, min_dist=0.8)
        data_cache['obsm_X_umap'][nb] = data.obsm['X_umap'].copy()
    
        # Do different clusterings on a given UMAP for a given nr of neighbors
        for res in resolutions:
            sc.tl.leiden(data, resolution=res)
            data_cache['leiden'][(nb, res)] = data.obs['leiden'].copy()

            # Compute and store silhouette coefficient
            data_cache['silhouette_score'][(nb, res)] = silhouette_score(data.obsm[X_name],data.obs['leiden'])


            # Compute and store dendrogram so can plot correlation matrix
            data_cache['dendrogram'][(nb, res)] = sc.tl.dendrogram(data, 
                groupby='leiden', n_pcs=data.uns['neighbors']['params']['n_pcs'],use_rep=X_name,inplace=False,key_added=dendrogram_name) 

    sc.settings.verbosity = 3 #reset
    
    return data_cache


'''
Plot comparisons for clustering parameters

Arguments:
    data: AnnData object
    data_cache (output of compare_clusterings_compute)
    marker (string: marker gene to plot on umap)
    show (boolean: whether to show figure in notebook) (default: True)
    save (filename to save figure as. If None (default), figure will not be saved)

In the resulting plot, each row is a set of parameters.
The first column shows the clusters on a UMAP, with the parameters and number of clusters listed above.
The second column plots the expression of the chosen marker on the UMAP, with the marker listed above.
The thrid column shows the correlation matrix of clusters with each other, with the silhouette coefficient listed above.
     The silhouette coefficient is a metric for evaluating unsupervised clustering.
     The silhouette coefficient is high when each data point has a small mean distance to points in its own cluster and a large mean distnce to points in its closest neighboring cluster 
     (we want points in a cluster to be similar to each other and distinct from points in other clusters).  
     The best possible score is 1. The worst possible score is -1.
     A score near 0 indicates overlapping clusters. Negative values generally indicate that samples are being assigned to the wrong cluster (they are more similar to points in a different cluster).
'''
#Robin's old code
# def compare_clusterings_plot(data, data_cache, marker, show=True, save=None):
#      #which embedding
#      embedding = data_cache['embedding']
#      if embedding == 'pca':
#          X_name = 'X_pca'    
#          dendrogram_name = 'dendrogram_leiden_noharmony'
    
#      elif embedding == 'harmony':
#          X_name = 'X_pca_harmony'
#          dendrogram_name = 'dendrogram_leiden_harmony'      

#      else:
#          raise ValueError("Unknown embedding! must be either 'pca' or 'harmony'")

#      nb_name = 'neighbors'
#      distances_name = 'distances'
#      connectivities_name = 'connectivities'

#      n_neighbors = data_cache['n_neighbors']
#      resolutions = data_cache['resolutions']

#      dotsize = 12
#      #one row for each set of parameters, three columns: umap with clusters labeled, umap of marker gene expression, correlation matrix of clusters
#      fig, axes = plt.subplots(nrows=len(n_neighbors)*len(resolutions), ncols=3, figsize=(15, 8*len(n_neighbors)*len(resolutions)))
    
#      i = 0 #which row
#      for nb in n_neighbors:
#          # Copy from cache to data
#          data.uns[nb_name] = data_cache['uns_neighbors'][nb].copy()
#          data.obsp[distances_name] = data_cache['obsp_distances'][nb].copy()
#          data.obsp[connectivities_name] = data_cache['obsp_connectivities'][nb].copy()
#          data.obsm['X_umap'] = data_cache['obsm_X_umap'][nb].copy()
    
#          for res in resolutions:
#              # Copy from cache to data
#              data.obs['leiden'] = data_cache['leiden'][(nb, res)].copy()
#              nc = len(data.obs["leiden"].unique())
#              data.uns['dendrogram_leiden'] = data_cache['dendrogram'][(nb, res)]

#              # Plot clusters on umap
#              sc.pl.umap(data, 
#                    color='leiden', 
#                    size=dotsize, 
#                    title=f'{nb} neighbors, {res} res: {nc} clusters', 
#                    legend_loc='on data', 
#                    legend_fontoutline=2,
#                    show=False, 
#                    ax=axes[i, 0]
#                    )

#              # Plot marker gene on umap
#              sc.pl.umap(data, 
#                    color=marker, 
#                    size=dotsize, 
#                    title=marker, 
#                    legend_loc='right', 
#                    #legend_fontoutline=2,
#                    show=False, 
#                    ax=axes[i, 1]
#                    )

#              # Plot correlation matrix
#              sc.pl.correlation_matrix(data, groupby='leiden',show_correlation_numbers=False, show=False, ax=axes[i,2])
#              axes[i,2].xaxis.set_visible(False)
#              axes[i,2].yaxis.set_visible(False)
#              # display silhouette coefficient above correlation matrix 
#              axes[i,2].set_title('Silhouette: ' + str(data_cache['silhouette_score'][(nb,res)]))

#              i+=1

#      if save is not None:
#          plt.savefig(save)
    
#      if show:
#         plt.show()


#new function from Anton:

# def compare_clusterings_plot(data, data_cache, markers, show=True,save=None):
#      #which embedding
#      embedding = data_cache['embedding']
#      if embedding == 'pca':
#          X_name = 'X_pca'    
#          dendrogram_name = 'dendrogram_leiden_noharmony'
    
#      elif embedding == 'harmony':
#          X_name = 'X_pca_harmony'
#          dendrogram_name = 'dendrogram_leiden_harmony'      

#      else:
#          raise ValueError("Unknown embedding! must be either 'pca' or 'harmony'")

#      nb_name = 'neighbors'
#      distances_name = 'distances'
#      connectivities_name = 'connectivities'

#      n_neighbors = data_cache['n_neighbors']
#      resolutions = data_cache['resolutions']




    #  fig, axes = plt.subplots(nrows=len(n_neighbors)*len(resolutions), ncols=3, figsize=(15, 8*len(n_neighbors)*len(resolutions)))
    
     
    #  dotsize = 12
    #  #one row for each set of parameters, three columns: umap with clusters labeled, umap of marker gene expression, correlation matrix of clusters
    #  nr = len(n_neighbors) * len(resolutions)  
    #  nm = 1 + len(markers)

    #  fig, axs = plt.subplots(nrows=nr, ncols=nm, figsize=(5 * nm, 4 * nr))
    
    #  i = 0 #which row
    #  for i, nb in enumerate(n_neighbors):
    #      # Copy from cache to data
    #      data.uns[nb_name] = data_cache['uns_neighbors'][nb].copy()
    #      data.obsp[distances_name] = data_cache['obsp_distances'][nb].copy()
    #      data.obsp[connectivities_name] = data_cache['obsp_connectivities'][nb].copy()
    #      data.obsm['X_umap'] = data_cache['obsm_X_umap'][nb].copy()
    
    #     #  for res in resolutions:
    #     #      # Copy from cache to data
    #     #      data.obs['leiden'] = data_cache['leiden'][(nb, res)].copy()
    #     #      nc = len(data.obs["leiden"].unique())
    #     #      data.uns['dendrogram_leiden'] = data_cache['dendrogram'][(nb, res)]

    #      for j, res in enumerate(resolutions):
            
    #         data.obs['leiden'] = data_cache['leiden'][(nb, res)].copy()
    #         nc = len(data.obs["leiden"].unique())
    #         data.uns['dendrogram_leiden'] = data_cache['dendrogram'][(nb, res)]
            
    #         row_idx = i * len(resolutions) + j

    #          # Plot clusters on umap
    #         sc.pl.umap(data, 
    #                color='leiden', 
    #                size=dotsize, 
    #                title=f'{nb} neighbors, {res} res: {nc} clusters', 
    #                legend_loc='on data', 
    #                legend_fontoutline=2,
    #                show=False, 
    #                ax=axs[row_idx, 0]
    #                )

    #         # Plot marker gene on umap
    #         for k, m in enumerate(markers):
    #             sc.pl.umap(
    #                 data,
    #                 color=m,
    #                 size=dotsize,
    #                 title=m,
    #                 vmin=0,
    #                 vmax=3,
    #                 cmap="bwr",
    #                 use_raw=True,
    #                 legend_loc="right",
    #                 # legend_fontoutline=2,
    #                 show=False,
    #                 ax=axs[row_idx, 1 + k],
    #             )
                
    #  if save is not None:
    #      plt.savefig(save)
    
    #  if show:
    #     plt.show()
'''
Load selected clustering stored in data_cache

Arguments:
    data: AnnData object
    data_cache: output of compare_clusterings_compute
    nb: chosen number of neighbors
    res: chosen resolution
'''
def load_clustering(data,data_cache,nb,res):
     embedding = data_cache['embedding']
     if embedding == 'pca':
         X_name = 'X_pca'  
         dendrogram_name = 'dendrogram_leiden_noharmony'  
     elif embedding == 'harmony':
         X_name = 'X_pca_harmony'
         dendrogram_name = 'dendrogram_leiden_harmony'
    
     nb_name = 'neighbors'
     distances_name = 'distances'
     connectivities_name = 'connectivities'
        
     # Copy from cache to data
     data.uns[nb_name] = data_cache['uns_neighbors'][nb].copy()
     data.obsp[distances_name] = data_cache['obsp_distances'][nb].copy()
     data.obsp[connectivities_name] = data_cache['obsp_connectivities'][nb].copy()
     data.obsm['X_umap'] = data_cache['obsm_X_umap'][nb].copy()
    
     # Copy from cache to data
     data.obs['leiden'] = data_cache['leiden'][(nb, res)].copy()
     data.uns[dendrogram_name] = data_cache['dendrogram'][(nb, res)]
     data.uns['dendrogram_leiden'] = data.uns[dendrogram_name] #whichever embedding was chosen, copy into the main dendrogram so it will be used in plotting
    


'''
Plot proportions of an attribute in whole dataset and chosen groups (ex. proportions of conditions in each cluster)

Arguments:
    data: AnnData object
    group_attribute: obs column by which you would like to group cells
    proportion_attribute: obs column whose proportions in each group you would like to visualize
'''

def plot_proportions(data,group_attribute,proportion_attribute):
    crosstable = pd.crosstab(data.obs[group_attribute], 
                 data.obs[proportion_attribute], 
                 margins=True, #include fractions for whole dataset
                 normalize='index'
                )
    #move margins to beginning
    crosstable = crosstable.reindex(['All'] + [x for x in crosstable.index if x != 'All'])

    #plot
    ax = crosstable.plot(kind='bar',
                       colormap = 'tab20',
                       stacked=True,
                       title='Relative '+proportion_attribute+' proportions in each '+group_attribute,
                       xlabel=group_attribute,
                       ylabel='fraction',
                       figsize=(6.4, 6.4)
                       )
    ax.spines.right.set_color('none')
    ax.spines.top.set_color('none')
    ax.legend(title=proportion_attribute, bbox_to_anchor=(1,1))

    plt.show()

'''
Read in differential expression results from edge2, plot a volcano plot labeling all significant genes

Arguments:
    csv_name: filepath to results from edgeR
    logFC_cutoff: absolute value of threshold for log-foldchange (default = 1)
    adj_pval_cutoff: threshold of adjusted p-value (default = 0.01)
'''
def volcano_plot_all_DE(csv_name,comparison_name,logFC_cutoff=1,adj_pval_cutoff=0.01):
    #read in csv made by Libra
    df = pd.read_csv(csv_name).set_index('Unnamed: 0')

    #color all upregulated genes
    plt.scatter(x=df['avg_logFC'],y=df['p_val_adj'].apply(lambda x:-np.log10(x)),s=1,color='grey')

    # highlight down- or up- regulated genes
    down = df[(df['avg_logFC']<=-logFC_cutoff)&(df['p_val_adj']<=adj_pval_cutoff)]
    up = df[(df['avg_logFC']>=logFC_cutoff)&(df['p_val_adj']<=adj_pval_cutoff)]
    DE = pd.concat([up,down])

    plt.scatter(x=DE['avg_logFC'],y=DE['p_val_adj'].apply(lambda x:-np.log10(x)),s=3,label="differentially expressed",color="blue")

    plt.xlabel("avg_logFC")
    plt.ylabel("-log_p_val_adj")
    plt.axvline(logFC_cutoff,color="black",linestyle="--")
    plt.axvline(-logFC_cutoff,color="black",linestyle="--")
    plt.axhline(-np.log10(adj_pval_cutoff),color="black",linestyle="--")
    plt.legend(loc='best')
    plt.title(comparison_name + ': ' + df['de_method'][1])


'''
Read in differential expression results from edge2, plot a volcano plot labeling selected genes

Arguments:
    csv_name: filepath to results from edgeR
    comparison_name: comparison name with which to title plot
    genes: list of genes to label
    logFC_cutoff: absolute value of threshold for log-foldchange (default = 1)
    adj_pval_cutoff: threshold of adjusted p-value (default = 0.01)
'''
def volcano_plot_label_genes(csv_name,comparison_name,genes,logFC_cutoff=1,adj_pval_cutoff=0.01):
    #read in csv made by Libra
    df = pd.read_csv(csv_name).set_index('Unnamed: 0')

    #color all upregulated genes
    plt.scatter(x=df['avg_logFC'],y=df['p_val_adj'].apply(lambda x:-np.log10(x)),s=1,color='grey')

    # highlight down- or up- regulated genes
    down = df[(df['avg_logFC']<=-logFC_cutoff)&(df['p_val_adj']<=adj_pval_cutoff)]
    up = df[(df['avg_logFC']>=logFC_cutoff)&(df['p_val_adj']<=adj_pval_cutoff)]
    DE = pd.concat([up,down])
    DE_IEGs = DE.loc[[x for x in DE.index if DE.gene[x] in IEGs]]
    label = df.loc[[x for x in df.index if df.gene[x] in genes]]


    plt.scatter(x=label['avg_logFC'],y=label['p_val_adj'].apply(lambda x:-np.log10(x)),s=3,color="red")

    #label upregulated IEGs
    #for i,r in label.iterrows():
        #plt.text(x=r['avg_logFC'],y=-np.log10(r['p_val_adj']),s=label.gene[i])
    print(label)
    #and save them
    #DE_IEGs.to_csv('odor_vs._noodor_upregulated_IEGs.csv')

    plt.xlabel("avg_logFC")
    plt.ylabel("-log_p_val_adj")
    plt.axvline(logFC_cutoff,color="black",linestyle="--")
    plt.axvline(-logFC_cutoff,color="black",linestyle="--")
    plt.axhline(-np.log10(adj_pval_cutoff),color="black",linestyle="--")
    plt.legend(loc='best')
    plt.title(comparison_name + ': ' + df['de_method'][1])


'''
Read in differential expression results from edge2, plot a volcano plot *for each cluster*, labeling all significant IEGs

Arguments:
    csv_name: filepath to results from edgeR
    comparison_name: comparison name with which to title plot
    logFC_cutoff: absolute value of threshold for log-foldchange (default = 1)
    adj_pval_cutoff: threshold of adjusted p-value (default = 0.01)

Returns: list of genes that are differentially expressed in at least one cluster
'''
## a different volcano plot for each cluster
## only differentially expressed IEGs

def volcano_plot_DE_IEGs_by_cluster(csv_name,comparison_name,logFC_cutoff=1,adj_pval_cutoff=0.01):
    plt.rcParams['font.size'] = 18

    #read in csv made by Libra
    full_df = pd.read_csv(csv_name).set_index('Unnamed: 0')

    fig, axs = plt.subplots(nrows=1,ncols=len(full_df.cell_type.unique()),figsize=(8*len(full_df.cell_type.unique()),8),sharex=True,sharey=True)

    DE_IEGs_list = []

    for c, cluster in enumerate(full_df.cell_type.unique()):

        df = full_df.loc[[x for x in full_df.index if full_df.cell_type[x] == cluster]]

        #color all upregulated genes
        axs[c].scatter(x=df['avg_logFC'],y=df['p_val_adj'].apply(lambda x:-np.log10(x)),s=1,color='grey')

        # highlight down- or up- regulated genes
        down = df[(df['avg_logFC']<=-logFC_cutoff)&(df['p_val_adj']<=adj_pval_cutoff)]
        up = df[(df['avg_logFC']>=logFC_cutoff)&(df['p_val_adj']<=adj_pval_cutoff)]
        DE = pd.concat([up,down])
        DE_IEGs = DE.loc[[x for x in DE.index if DE.gene[x] in IEGs]]

        axs[c].scatter(x=DE_IEGs['avg_logFC'],y=DE_IEGs['p_val_adj'].apply(lambda x:-np.log10(x)),s=5,label="differentially expressed IEGs",color="red")

        #label upregulated IEGs
        for i,r in DE_IEGs.iterrows():
            axs[c].text(x=r['avg_logFC'],y=-np.log10(r['p_val_adj']),s=DE_IEGs.gene[i],fontsize='large')
        DE_IEGs_list.append(DE_IEGs) #for printing
        #and save them
        #DE_IEGs.to_csv('odor_vs._noodor_upregulated_IEGs.csv')

        axs[c].set_xlabel("avg_logFC")
        axs[c].set_ylabel("-log_p_val_adj")
        axs[c].axvline(logFC_cutoff,color="black",linestyle="--")
        axs[c].axvline(-logFC_cutoff,color="black",linestyle="--")
        axs[c].axhline(-np.log10(adj_pval_cutoff),color="black",linestyle="--")
        #axs[c].legend(loc='best')
        axs[c].set_title(comparison_name + ': ' + df['de_method'].to_list()[0] + ': ' + str(cluster))

    print(pd.concat(DE_IEGs_list))
    plt.show()
    return pd.concat(DE_IEGs_list).gene.to_list()
